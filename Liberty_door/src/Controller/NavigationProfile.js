/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { createStackNavigator } from 'react-navigation';

import ProfileScreen from '../View/ProfileScreen';
import PostScreen    from '../View/PostScreen';

const NavigationProfile = createStackNavigator (
  {
    Root: ProfileScreen,
    Post: PostScreen,
  },
  {
    initialRouteName: 'Root',
  }
);

export default NavigationProfile;

