/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { YellowBox } from 'react-native';

import Navigation from './Navigation';

// note : react-navigationのライブラリ使用によるWarningを無効化する.
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class Launch extends Component<Props> {
  render() {
    return (
      <Navigation />
    )
  }
}
