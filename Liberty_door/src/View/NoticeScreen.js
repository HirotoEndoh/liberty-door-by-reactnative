/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class NoticeScreen extends Component<Props> {
  static navigationOptions = {
      title: 'Notice',
  };

  render() {
    return (
      <View style={styles.container}>
        // ******************************************
        // * トップメッセージ
        // ******************************************
        <Text style={styles.welcome}>
          {'Notice Page'}
          {'\n'}
          {'Coming soon...'}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
