/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
} from 'react-native';
import { Button }  from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

export default class PostScreen extends Component<Props> {
  constructor(props){
    super(props);
    this.state = {image_source: null}
  }

  static navigationOptions = {
    title: 'Post',
  };

  _actionImgPicker = () => {
    let img_pick_options = {
      title: 'Select Avatar',
      storageOptions: { skipBackup: true, path: 'images', },
    }

    ImagePicker.showImagePicker(img_pick_options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker')
    
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
    
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
    
      } else {
        let source = { uri: response.uri }
        this.setState({ image_source: source })
      }
    })
  };

  render() {
    var imageData = null

    if (this.state.image_source != null) {
      imageData = <Image style  = {{ width: 300, height: 100, }}
                         source = {this.state.image_source}
                  />
    }

    return (
      <View style={styles.container}>
        // ******************************************
        // * トップメッセージ
        // ******************************************
        <Text style={styles.welcome}>
          {'Post Page'}
          {'\n'}
        </Text>

        // ******************************************
        // * 文字入力欄
        // ******************************************
        <TextInput style={styles.textInput} />

        // ******************************************
        // * 画像
        // ******************************************
        {imageData}

        // ******************************************
        // * 画像ボタン
        // ******************************************
        <Button
          title       = '画像'
          buttonStyle = {styles.button}
          onPress     = {this._actionImgPicker}
        />

        // ******************************************
        // * 投稿ボタン
        // ******************************************
        <Button
          title       = '投稿'
          buttonStyle = {styles.button}
          onPress     = {() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textInput: {
    backgroundColor: '#ABC',
    marginRight: 50,
    marginLeft: 50,
    height: 150,
  },
  button: {
    backgroundColor: "rgba(92, 99,216, 1)",
    height: 45,
    marginTop: 50,
    marginRight: 50,
    marginLeft: 50,
  },
});
