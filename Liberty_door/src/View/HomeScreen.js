/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  FlatList,
  Text,
  View,
} from 'react-native';

import firebase from 'react-native-firebase'

function _database_read(arg)
{
  return new Promise(function (resolve, reject) {
    firebase.database()
      .ref('user/test_user_02/posts')
      .on('value', (snapshot) => { resolve([arg, snapshot]) });
  });
};

export default class HomeScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  static navigationOptions = {
      title: 'Home',
  };

  _setPostData(data) {
    component = data[0]
    snapshot = data[1]
    db = firebase.database()

    Promise.all(Object.keys(snapshot.val()).map(function (post_id) {
      return new Promise(function (resolve, reject) {
        dbRef = db.ref('post/data/' + post_id)
        dbRef.on('value', (snapshot) => { resolve(snapshot.val()) });
      });
    }))
    .then(function (postData) {
      textIds = []

      for (let i in postData) {
        textIds.push(postData[i]['text'])
      }

      return Promise.all(textIds.map(function (textId) {
        return new Promise(function (resolve, reject) {
          db_ref = db.ref('post/resource/text/' + textId)
          db_ref.on('value', (text) => { resolve(text.val()) });
        });
      }))
    })
    .then(function (texts) {
      dispData = []

      for (let i in texts) {
        dispData.push({ key: texts[i] })
      }

      component.setState({ data: dispData })
    });
  }

  componentWillMount()
  {
    _database_read(this).then(this._setPostData)
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          // ******************************************
          // * 投稿内容
          // ******************************************
          <FlatList
            data         = {this.state.data}
            extraData    = {this.state.data}
            keyExtractor = {this._keyExtractor}
            renderItem   = {({item}) =>
              <Text style={styles.movieView} onPress={()=>this.props.navigation.navigate('')} >
                  {'メッセージ'}
                  {'\n\t'}
                  {item.key}
              </Text>}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  body: {
    flex: 1,
  },
  movieView: {
    padding: 10,
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderWidth: 1,
  },
});

