/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

export default class ProfileScreen extends Component<Props> {
  static navigationOptions = {
      title: 'Profile',
  };

  render() {
    return (
      <View style={styles.container}>
        // ******************************************
        // * トップメッセージ
        // ******************************************
        <Text style={styles.welcome}>
          {'ひろとくんお誕生日おめでとう！！！'}
        </Text>

        // ******************************************
        // * 投稿ボタン
        // ******************************************
        <Button
          title   = '祝う'
          onPress = {()=>this.props.navigation.navigate('Post')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
