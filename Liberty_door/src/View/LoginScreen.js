/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

// ******************************************
// * Facebookログイン
// ******************************************
var FBLoginButton = require('./../Controller/LDFacebook');
const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
} = FBSDK;

let window = null

LoginManager.logInWithReadPermissions(['public_profile']).then(
  function(result) {
    if (result.isCancelled) { 
      alert('Login was cancelled');
    } else {
      window.props.navigation.navigate('Main');
    }
  },
  function(error) {
    alert('Login failed with error: ' + error);
  }
);

export default class LoginScreen extends Component<Props> {
  static navigationOptions = {
      title: 'Login',
  };

  render() {
    window = this
    return (
      <View style={styles.container}>
        //Facebookログインボタン
        <FBLoginButton />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
