/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { createStackNavigator } from 'react-navigation';

import NoticeScreen from '../View/NoticeScreen';

const NavigationNotice = createStackNavigator (
  {
    Root: NoticeScreen,
  },
  {
    initialRouteName: 'Root',
  }
);

export default NavigationNotice;

