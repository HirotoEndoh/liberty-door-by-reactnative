/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { createStackNavigator } from 'react-navigation';

import HomeScreen from '../View/HomeScreen';

const NavigationHome = createStackNavigator (
  {
    Root  : HomeScreen,
    Detail: HomeScreen,
  },
  {
    initialRouteName: 'Root',
  }
);

export default NavigationHome;
