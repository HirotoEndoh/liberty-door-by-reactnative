/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { createBottomTabNavigator } from 'react-navigation';

import LoginScreen       from './../View/LoginScreen';
import NavigationHome    from './NavigationHome';
import NavigationNotice  from './NavigationNotice';
import NavigationProfile from './NavigationProfile';

const MainNavigation = createBottomTabNavigator (
  {
    Home   : NavigationHome,
    Notice : NavigationNotice,
    Profile: NavigationProfile,
  },
  {
    initialRouteName: 'Profile',
  }
);

const Navigation = createBottomTabNavigator (
  {
    Login : LoginScreen,
    Main  : MainNavigation,
  },
  {
    initialRouteName  : 'Login',
    navigationOptions : { tabBarVisible: false }
  }
);

export default Navigation;
